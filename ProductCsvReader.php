<?php
/*
 * Loads in a CSV File and allows you to fetch groups of products that have the same ID
 */
class ProductCsvReader {
    protected $products = array();

    /* 
     * Parses CSV and populates product array
     * Loading full CSV into memory may be a problem when dealing with huge CSV's.
     * Filename: e.g. files/product-data.csv
     * Cols: e.g. array('id','brand','name')
     */
    public function __construct($filename, $cols){
        $fh = fopen($filename, 'r');
        while($line = fgetcsv($fh)){
            $product = array();
            // Convert array to an associative array thats easier to work with
            foreach($cols as $index => $col){
                $product[$col] = $line[$index];
            }
            $this->products[] = $product;
        }
    }

    /*
     * Do we have any products left?
     */
    public function hasProducts(){
        return count($this->products) > 0;
    }

    /*
     * Gets next batch of products that have the same Product ID
     */
    public function getNextProductBatch(){
        $batch = array();

        while(count($this->products) > 0){
            $product = array_shift($this->products);
            // If first product or SKU is the same as the last product
            if(count($batch) == 0 || $product['id'] == $batch[0]['id']){
                $batch[] = $product;
            }
            else
            {
                // Put the product back in the 'queue' and return the current batch
                array_unshift($this->products, $product);
                return $batch;
            }
        }

        return $batch;
    }
}
