<?php
require_once 'vendor/autoload.php';
require_once 'ProductCsvReader.php';

use DemandwareXml\Product;
use DemandwareXml\Document;

$document = new Document('TestCatalog');

// See ProductCsvReader.php for CSV parsing code which returns batches of products that have the same ID.
$reader = new ProductCsvReader('files/product-data.csv', array('id','brand','name','barcode','color','size','default'));

while($reader->hasProducts()){

    // This returns a batch of products that all have the same ID.
    $products = $reader->getNextProductBatch();

    $product = new Product($products[0]['id']);
    $product->setName($products[0]['name']);
    $product->setBrand($products[0]['brand']);

    $addVariantsLater = array(); // Stores variant objects
    $productVariants = array(); // Stores variants and their default status

    foreach($products as $variantData){
        $variantId = $variantData['barcode'];

        $variant = new Product($variantId);
        $variant->setCustomAttributes(array(
            'color' => $variantData['color'],
            'size' => $variantData['size'],
        ));

        $productVariants[$variantId] = $variantData['default'] == 'Y'? true : false;

        // Don't add variants to document before adding product (To match expected-output.xml as much as possible)
        $addVariantsLater[] = $variant;
    }

    $product->setSharedAttributes(['color', 'size']); // Variants weren't being added without this
    $product->setVariants($productVariants);
    $document->addObject($product);

    foreach($addVariantsLater as $addVariantLater){
        $document->addObject($addVariantLater);
    }
}

// Output XML
$xml = $document->getDomDocument()->saveXML();
$fh = fopen('files/actual-result.xml', 'w');
fwrite($fh, $xml);
fclose($fh);
echo $xml;

